//package com.xh.excel;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.xssf.streaming.SXSSFWorkbook;
//
//import java.io.FileOutputStream;
//import java.sql.*;
//
///**
// * 类说明
// *
// * @author yangyangang
// * @version V1.0 创建时间: 2019/6/4 16:00 Copyright 2019 by WiteMedia
// */
//@Slf4j
//public class MillionExportToExcelUtilBlk {
//    /**
//     * 数据库连接操作
//     */
//    public Connection getConnection() throws Exception {
//        // 使用jdbc链接数据库
//        Class.forName("com.mysql.jdbc.Driver").newInstance();
//        String url = "jdbc:mysql://localhost:3306/test_excel?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=GMT";
//        String username = "root";
//        String password = "123456";
//        // 获取数据库连接
//        Connection conn = DriverManager.getConnection(url, username, password);
//        return conn;
//    }
//
//    /**
//     * @return boolean
//     * @description 执行导出Excel操作
//     * @author yangyangang 2019/6/4 16:04
//     */
//    public boolean WriteExcel(boolean isClose, int count, String fileds, String tableName, int limits) {
//        String excelFileName = "E:/test.xlsx";
//        // 内存中只创建100个对象，写临时文件，当超过100条，就将内存中不用的对象释放。
//        SXSSFWorkbook wb = new SXSSFWorkbook(100);
//        Sheet sheet = null; // 工作表对象
//        Row nRow = null; // 行对象
//        Cell nCell = null; // 列对象
//        try {
//            Connection conn = getConnection();
//            Statement stmt = conn.createStatement();
//            String sql = "select " + fileds + " from " + tableName + " limit " + limits;
//            log.info("==================SQL: " + sql);
//            ResultSet rs = stmt.executeQuery(sql); // 获取执行结果
//            ResultSetMetaData rsmd = rs.getMetaData(); // 获取执行结果的结构(rs.getMetaData().getTableName(1))就可以返回表名,rs.getMetaData().getColumnCount())
//
//            long startTime = System.currentTimeMillis();
//            log.info("==================开始执行时间 : " + startTime / 1000 + "m");
//            int rowNo = 0; // 总行号
//            int pageRowNo = 0; // 页行号
//
//            while (rs.next()) {
//                // 打印count条后切换到下个工作表，可根据需要自行拓展，2百万，3百万...数据一样操作，只要不超过1048576就可以
//                if (rowNo % count == 0) {
//                    log.info("==================当前sheet页为:" + rowNo / count);
//                    sheet = wb.createSheet("我的第" + (rowNo / count + 1) + "个工作簿"); // 建立新的sheet对象
//                    sheet = wb.getSheetAt(rowNo / count); // 动态指定当前的工作表
//                    pageRowNo = 1; // 每当新建了工作表就将当前工作表的行号重置为1
//
//                    //定义表头
//                    nRow = sheet.createRow(0);
//                    Cell cel0 = nRow.createCell(0);
//                    cel0.setCellValue("第一行");
//                    Cell cel2 = nRow.createCell(1);
//                    cel2.setCellValue("第二行");
//                    Cell cel3 = nRow.createCell(2);
//                    cel3.setCellValue("第三行");
//                    Cell cel4 = nRow.createCell(3);
//                    cel4.setCellValue("第四行");
//                    Cell cel5 = nRow.createCell(4);
//                    cel5.setCellValue("第五行");
//                    Cell cel6 = nRow.createCell(5);
//                    cel6.setCellValue("第六行");
//                }
//                rowNo++;
//                nRow = sheet.createRow(pageRowNo++); // 新建行对象
//
//                // 打印每行，每行有6列数据 rsmd.getColumnCount()==6 --- 列属性的个数
//                for (int i = 0; i < rsmd.getColumnCount(); i++) {
//                    nCell = nRow.createCell(i);
//                    nCell.setCellValue(rs.getString(i + 1));
//                }
//
//                if (rowNo % 10000 == 0) {
//                    log.info("==================" + rowNo);
//                }
//            }
//
//            long finishedTime = System.currentTimeMillis(); // 处理完成时间
//            log.info("==================数据读取完成耗时 : " + (finishedTime - startTime) / 1000 + "m");
//
//            FileOutputStream fOut = new FileOutputStream(excelFileName);//将数据写入Excel
//            wb.write(fOut);
//            fOut.flush(); // 刷新缓冲区
//            fOut.close();
//
//            long stopTime = System.currentTimeMillis(); // 写文件时间
//            log.info("==================数据写入Excel表格中耗时 : " + (stopTime - startTime) / 1000 + "m");
//
//            if (isClose) {
//                this.close(rs, stmt, conn);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//    // 执行关闭流的操作
//    private void close(ResultSet rs, Statement stmt, Connection conn) throws SQLException {
//        rs.close();
//        stmt.close();
//        conn.close();
//    }
//
//    //测试方法
//    public static void main(String[] args) {
//        MillionExportToExcelUtilBlk bdeo = new MillionExportToExcelUtilBlk();
//        String fileds = "id,user_id,vote_id,group_id,create_time";
//        String tableName = "vote_record_memory";
//        int limit = 100000;
//        bdeo.WriteExcel(true, 10000, fileds, tableName, limit);
//    }
//
//}
