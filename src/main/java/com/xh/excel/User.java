package com.xh.excel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Name User
 * @Description
 * @Author wen
 * @Date 2019-06-04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private int id;

    private String user_id;

    private int vote_id;

    private int group_id;

    private String create_time;
}
